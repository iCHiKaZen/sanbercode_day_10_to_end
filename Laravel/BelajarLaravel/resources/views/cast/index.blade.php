@extends('layout.master')
@section('judul')
    Halaman Tampil Data
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm my-2 ">Tambah Data Baru</a>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key =>$item ) 
                <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$item->nama}} </td>
                    <td> {{$item->umur}} </td>
                    <td> 
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sn">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sn">Edit</a>
                    </td>
                </tr>
            @empty
                
            @endforelse
        </tbody>
      </table>
@endsection