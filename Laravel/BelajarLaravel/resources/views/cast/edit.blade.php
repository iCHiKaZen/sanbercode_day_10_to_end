@extends('layout.master')
@section('judul')
    Halaman Edit Data
@endsection

@section('content')
<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama Lengkap :</label>
      <input type="text" value="{{$cast->nama}}" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur :</label>
      <input type="text" value="{{$cast->umur}}" class="form-control" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio :</label>
        <textarea name="bio" id="bio" cols="30" rows="10" class="form-control" >{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection