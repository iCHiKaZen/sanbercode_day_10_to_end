@extends('layout.master')
@section('judul')
    Halaman Tambah Data
@endsection
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Pendaftaran Pemain Film Baru</title>
</head>
<body>
    <form method="POST" action="/cast">
        @csrf
        <div class="form-group">
          <label>Nama Lengkap :</label>
          <input type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label>Umur :</label>
          <input type="text" class="form-control" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio :</label>
            <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</body>
</html>
@endsection
