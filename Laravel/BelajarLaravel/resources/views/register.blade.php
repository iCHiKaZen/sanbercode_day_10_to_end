<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="FiName" id="FiName"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="LaName" id="LaName"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" id="gender">Man<br>
        <input type="radio" name="gender" id="gender">Women<br>
        <input type="radio" name="gender" id="gender">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="nationality">Indonesian</option>
            <option value="nationality">Singaporean</option>
            <option value="nationality">Malaysian</option>
            <option value="nationality">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="laspok" id="laspok">Bahasa Indonesia <br>
        <input type="checkbox" name="laspok" id="laspok">English <br>
        <input type="checkbox" name="laspok" id="laspok">Arabic <br>
        <input type="checkbox" name="laspok" id="laspok">Japanese<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>