<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class homeController extends Controller
{
    public function home()
    {
        return view ('home');
    }

    public function register()
    {
        return view ('register');
    }

    public function welcome(Request $request)
    {
        $FiNama = $request['FiName'];
        $LaNama = $request['LaName'];
        return view('welcome',['namaDepan'=>$FiNama, 'namaBelakang'=>$LaNama]);
    }
}
