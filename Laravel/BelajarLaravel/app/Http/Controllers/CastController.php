<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        //validasi data
        $request->validate([
            'nama'=> 'required',
            'umur'=> 'required|numeric',
            'bio'=> 'required|min:30',
        ]);
        //masukkan data ke database
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        //lempar ke halaman /index
        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', ['cast' => $cast]);
    }

    //Show tidak jalan
    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail',['cast'->$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit',['cast'->$cast]);
    }
}
