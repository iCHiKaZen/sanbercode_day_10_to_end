<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[homeController::class,'home']);

Route::get('/register',[homeController::class,'register']);

Route::post('/welcome',[homeController::class,'welcome']);

Route::get('/dashboard',function(){
    return view('Dashboard');
});

Route::get('/table',function(){
    return view('table');
});

Route::get('data-tables',function(){
    return view('data-tables');
});

Route::get('/cast/create',[CastController::class, 'create']);

Route::post('/cast',[CastController::class,'store']);

Route::get('/cast',[CastController::class,'index']);

Route::get('/cast/{id}',[CastController::class,'show']);

Route::get('/cast/{id}/edit',[CastController::class.'edit']);
